<?php

namespace App\Http\Controllers;


use App\Models\BorrowedBook;
use App\Models\Book;
use App\Models\ReturnedBook;
use App\Http\Requests\BorrowRequest;
use Illuminate\Http\Request;


class BorrowedBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Borrowedbook = Borrowedbook::all();
        return response()->json([
            "message" => "Borrwed Books",
            "data" => $Borrowedbook]);
    }

    public function store(BorrowRequest $request)
    {
        $borrowedbook = new BorrowedBook();
        $borrowedbook->copies = $request->copies;
        $borrowedbook->book_id = $request->book_id;
        $borrowedbook->patron_id = $request->patron_id;

        $Books = Book::find($borrowedbook->book_id);

        $minuscopies = $Books->copies - $borrowedbook->copies;
        

        $validated = $request->validated();

        $borrowedbook->save();
        $Books->update(['copies' => $minuscopies]);
        return response()->json(['message' => "Borrowed Book Success",
        "data" => $borrowedbook, $Books]);

    }

    public function show($id)
    {
        $borrowedbook = BorrowedBook::find($id);
        return response()->json($borrowedbook);

    }

    public function update(Request $request, $id)
    {
        $borrowedbook = BorrowedBook::find($id);
        $returnedbook = new ReturnedBook();

        $returnedbook->copies = $request->copies;
        $returnedbook->book_id = $request->book_id;
        $returnedbook->patron_id = $request->patron_id;

        $Books = Book::find($returnedbook->book_id);

        $addcopiestobook = $Books->copies + $returnedbook->copies;

        $returnedbook->save();
        $returnedbook->delete();
        $Books->update(['copies' => $addcopiestobook]);
        return response()->json(["message" => "Borrowed Book success",
        "data" => $borrowedbook, $Books, $returnedbook]);


    }

}
