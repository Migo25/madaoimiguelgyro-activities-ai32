<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    $multiples_row = [
    [ 'category' => 'Action'],
    [ 'category' => 'Short Story'],
    [ 'category' => 'Drama'],
    [ 'category' => 'Love Story'],
    [ 'category' => 'Horror'],    
    ];   

    foreach ($multiples_row as $rows) {
        Category::create($rows);
    }
 }
}